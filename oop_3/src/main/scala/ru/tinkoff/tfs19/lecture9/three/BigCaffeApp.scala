package ru.tinkoff.tfs19.lecture9.three

import scala.concurrent.{Future, Await}
import scala.concurrent.duration.Duration
import scala.util.Random
import concurrent.ExecutionContext.Implicits.global
import FutureImplicits._

object FutureImplicits {
  implicit class FutureExtension[T](val f: Future[T]) extends AnyVal {
    def await(): T = Await.result(f, Duration.Inf)
  }
}

case class Order(tableNumber: Int, items: Seq[String]) {
  override def toString: String = items.mkString(",")
}

case class FinishedOrder(order: Order)

class Waiter(val name: String) {
  say(s"Жду заказов...😴")


  def makeOrder(client: Client, items: Seq[String]): FinishedOrder = {
    val order = Order(client.tableNumber, items)
    putInProgress(order)
    items.foreach { item =>
      Thread.sleep(10)
      say(s"$item готово")
    }
    val finishedOrder = FinishedOrder(order)
    putInFinished(finishedOrder)
    finishedOrder
  }
  def getOrderInProgress(): Map[Int, Order] = {
    say(s"Согласно моим записям мне осталось обслужить ${ordersInProgress.size} клиентов")
    ordersInProgress
  }

  def getFinishedOrders(): Map[Int, FinishedOrder] = {
    say(s"Согласно моим записям я обслужил ${finishedOrders.size} клиента")
    finishedOrders
  }

  private def putInProgress(order: Order): Unit = {
    ordersInProgress += (order.tableNumber -> order)
  }
  private def putInFinished(finishedOrder: FinishedOrder): Unit = {
    ordersInProgress -= finishedOrder.order.tableNumber
    finishedOrders += (finishedOrder.order.tableNumber -> finishedOrder)
  }

  @volatile private var ordersInProgress = Map[Int, Order]()
  @volatile private var finishedOrders = Map[Int, FinishedOrder]()

  private def say(what: String): Unit = println(s"$name: $what")

}



class Client(name: String, val tableNumber: Int) {

  def eat(finishedOrder: FinishedOrder): Unit = {
    finishedOrder.order.items.foreach { it => say(s"Om-nom-nom $it")}
  }

  def timeForDinner(): Unit = {
    val waiter = Caffe.getWaiter
    val order = Random.shuffle(Caffe.items).take(3)
    say(s"Мне пожалуйста $order")
    val finishedOrder: FinishedOrder = waiter.makeOrder(this, order)
    eat(finishedOrder)
    say("Счет! 💵")
  }

  say(s"Хм. Может съесть 🍔?...")

  def say(what: String): Unit = println(s"$name: $what")
}

object Caffe {
  def getWaiter: Waiter = garry

  def showStatistics(): Unit = {
    garry.getFinishedOrders()
    garry.getOrderInProgress()
  }

  val items: Seq[String] = Seq("🍻", "🍺", "🍖", "🍗", "🥓", "🍔", "🍄", "🍷", "🥃")
  private val garry: Waiter = new Waiter("Garry")
}

object BigCaffeApp extends App {
  val clients = (1 to 100).map(nextClient).map(client => Future(client.timeForDinner()))
  Future.sequence(clients).await()

  Caffe.showStatistics()

  private def nextClient(table: Int): Client = new Client(Random.nextString(5), table)
}
