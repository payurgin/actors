name := "actors"

version := "0.1"

scalaVersion := "2.12.8"

lazy val domain = project
lazy val oop_1 = project
lazy val oop_2 = project
lazy val oop_3 = project

lazy val actor_1 = project
  .settings(
      libraryDependencies ++= Seq(
          "com.typesafe.akka" %% "akka-actor-typed" % "2.5.22"
      )
  )

lazy val actor = project
  .settings(
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-actor-typed" % "2.5.22"
    )
  )

lazy val root = (project in file("."))
  .aggregate(
    domain,
    oop_1,
    oop_2,
    oop_3,
    actor,
    actor
  )