package ru.tinkoff.tfs19.lecture9.actor

import akka.actor.Scheduler

import scala.concurrent.{Future, Await}
import scala.concurrent.duration._
import scala.util.Random
import scala.concurrent.ExecutionContext.Implicits.global
import akka.actor.typed.{Signal, Behavior, ActorSystem, ActorRef, PostStop}
import akka.actor.typed.scaladsl.{ActorContext, AbstractBehavior, Behaviors}
import akka.actor.typed.scaladsl.AskPattern._
import akka.util.Timeout
import ru.tinkoff.tfs19.lecture9.actor.Caffe._

case class Order(tableNumber: Int, items: Seq[String]) {
  override def toString: String = items.mkString(",")
}

case class FinishedOrder(order: Order)


object WaiterActor {
  sealed trait Command
  case class MakeOrder(client: Client, items: Seq[String], replyTo: ActorRef[FinishedOrder]) extends Command
  case class GetOrderInProgress(replyTo: ActorRef[Int]) extends Command
  case class GetFinishedOrders(replyTo: ActorRef[Int]) extends Command

  def apply(name: String): Behavior[Command] = Behaviors.setup(context => new WaiterActor(name, context))
}

class WaiterActor(name: String, context: ActorContext[WaiterActor.Command]) extends AbstractBehavior[WaiterActor.Command] {
  say(s"Жду заказов...😴")

  override def onMessage(msg: WaiterActor.Command): Behavior[WaiterActor.Command] = msg match {
    case WaiterActor.MakeOrder(client, items, replyTo) =>
      val order = Order(client.tableNumber, items)
      putInProgress(order)
      items.foreach { item =>
        Thread.sleep(10)
        say(s"$item готово")
      }
      val finishedOrder = FinishedOrder(order)
      putInFinished(finishedOrder)
      replyTo.tell(finishedOrder)
      Behaviors.same

    case WaiterActor.GetFinishedOrders(replyTo) =>
      say(s"Согласно моим записям я обслужил ${finishedOrders.size} клиента")
      replyTo ! finishedOrders.size
      Behaviors.same

    case WaiterActor.GetOrderInProgress(replyTo) =>
      say(s"Согласно моим записям мне осталось обслужить ${ordersInProgress.size} клиентов")
      replyTo ! ordersInProgress.size
      Behaviors.same
  }


  override def onSignal: PartialFunction[Signal, Behavior[WaiterActor.Command]] = {
    case PostStop =>
      say("Смена окончена 🕺")
      this
  }


  def getOrderInProgress(): Map[Int, Order] = {
    say(s"Согласно моим записям мне осталось обслужить ${ordersInProgress.size} клиентов")
    ordersInProgress
  }

  def getFinishedOrders(): Map[Int, FinishedOrder] = {
    say(s"Согласно моим записям я обслужил ${finishedOrders.size} клиента")
    finishedOrders
  }

  private def putInProgress(order: Order): Unit = {
    ordersInProgress += (order.tableNumber -> order)
  }
  private def putInFinished(finishedOrder: FinishedOrder): Unit = {
    ordersInProgress -= finishedOrder.order.tableNumber
    finishedOrders += (finishedOrder.order.tableNumber -> finishedOrder)
  }

  private var ordersInProgress = Map[Int, Order]()
  private var finishedOrders = Map[Int, FinishedOrder]()

  private def say(what: String): Unit = println(s"$name: $what")

}

class Client(name: String, val tableNumber: Int) {

  def eat(finishedOrder: FinishedOrder): Unit = {
    finishedOrder.order.items.foreach { it => say(s"Om-nom-nom $it")}
  }

  def timeForDinner(): Future[Unit] = {
    for {
      waiter <- Caffe.getWaiter
      items = Random.shuffle(Caffe.items).take(3)
      _ = say(s"Мне пожалуйста $items")
      finishedOrder <- waiter.ask[FinishedOrder](ref => WaiterActor.MakeOrder(this, items, ref))
      _ = eat(finishedOrder)
      _ = say("Счет! 💵")
    } yield ()
  }

  say(s"Хм. Может съесть 🍔?...")

  def say(what: String): Unit = println(s"$name: $what")
}

object Caffe {
  def getWaiter: Future[ActorRef[WaiterActor.Command]] = getHurry

  def showStatistics(): Future[Unit] = {
    for {
      hurry <- getHurry
      _ <- hurry.ask[Int](r => WaiterActor.GetFinishedOrders(r))
      _ <- hurry.ask[Int](r => WaiterActor.GetOrderInProgress(r))
    } yield ()
  }

  val items: Seq[String] = Seq("🍻", "🍺", "🍖", "🍗", "🥓", "🍔", "🍄", "🍷", "🥃")
  val actorSystem: ActorSystem[Nothing] = ActorSystem[Nothing](CaffeSupervisor(), "caffe-systen")
  implicit val timeout: Timeout = 3.seconds
  implicit val scheduler: Scheduler = Caffe.actorSystem.scheduler

  private val getHurry = actorSystem.systemActorOf(WaiterActor("Hurry"), "Hurry")
}

object CaffeSupervisor {
  def apply(): Behavior[Nothing] =
    Behaviors.setup[Nothing](context => new CaffeSupervisor(context))
}

class CaffeSupervisor(context: ActorContext[Nothing]) extends AbstractBehavior[Nothing] {
  override def onMessage(msg: Nothing): Behavior[Nothing] = Behaviors.unhandled

  override def onSignal: PartialFunction[Signal, Behavior[Nothing]] = {
    case PostStop =>
      println("Кафе закрыто")
      this
  }
}

object BigCaffeActorApp extends App {
  for {
    _ <- Future.sequence((1 to 50).map(nextClient).map(client => client.timeForDinner()))
    _ <- Caffe.showStatistics()
  } yield ()

  private def nextClient(table: Int): Client = new Client(Random.nextString(5), table)
}

object FutureImplicits {
  implicit class FutureExtension[T](val f: Future[T]) extends AnyVal {
    def await: T = Await.result(f, Duration.Inf)
  }

}
