package ru.tinkoff.tfs19.lecture9.one

import scala.util.Random

case class Order(tableNumber: Int, items: Seq[String]) {
  override def toString: String = items.mkString(",")
}

case class FinishedOrder(order: Order)

class Waiter(val name: String) {
  def makeOrder(client: Client, items: Seq[String]):  FinishedOrder = {
    val order = Order(client.tableNumber, items)
    ordersInProgress = ordersInProgress + (order.tableNumber -> order)
    items.foreach { it =>
      say(s"Готовим $it")
      Thread.sleep(10)
    }
    ordersInProgress -= order.tableNumber
    val finishedOrder = FinishedOrder(order)
    finishedOrders = finishedOrders + (order.tableNumber -> finishedOrder)
    finishedOrder
  }

  def getMenu(): Seq[String] = Caffe.items

  say(s"Жду заказов...😴")

  private var ordersInProgress = Map[Int, Order]()
  private var finishedOrders = Map[Int, FinishedOrder]()

  private def say(what: String): Unit = println(s"$name: $what")

}

class Client(name: String, val tableNumber: Int) {
  def timeToDinner() = {
    val waiter = Caffe.getWaiter
    val menu = waiter.getMenu()
    val items = Random.shuffle(menu).take(3)
    say(s"Я хочу $items")
    val order = waiter.makeOrder(this, items)
    order.order.items.foreach(it => say(s"Omnonon $it"))
  }

  say(s"Хм. Может съесть 🍔?...")

  def say(what: String): Unit = println(s"$name: $what")
}

object Caffe {
  def getWaiter: Waiter = Hurry

  val items: Seq[String] = Seq("🍻", "🍺", "🍖", "🍗", "🥓", "🍔", "🍄", "🍷", "🥃")
  private val Hurry: Waiter = new Waiter("Garry")
}



object EmptyCaffeApp extends App {
  val fedor = new Client("Fedor", 1)
  fedor.timeToDinner()
}